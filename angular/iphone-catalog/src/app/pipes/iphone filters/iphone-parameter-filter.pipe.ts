import { Pipe, PipeTransform } from '@angular/core';
import {iPhoneColor, iPhoneCondition, iPhoneHasTwoSim, iPhoneMemory, iPhoneModel} from '../../parameters/iphoneParameters';
import {IIphoneProduct} from '../../interfaces/iphoneProduct';
import {HelpingService} from '../../services/helping.service';

@Pipe({
  name: 'iphoneParameterFilter'
})

export class IphoneParameterFilterPipe implements PipeTransform {
  constructor(public helpingService: HelpingService) {}
  transform(
    items: IIphoneProduct[],
    modelFilters: string[],
    conditionFilters: string[],
    colorFilters: string[],
    memoryFilters: number[],
    has2simFilters: string[]): IIphoneProduct[] {
    let filteredItems = items;
    if (modelFilters.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, modelFilters, iPhoneModel);
    }
    if (conditionFilters.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, conditionFilters, iPhoneCondition);
    }
    if (colorFilters.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, colorFilters, iPhoneColor);
    }
    if (memoryFilters.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, memoryFilters, iPhoneMemory);
    }
    if (has2simFilters.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, has2simFilters, iPhoneHasTwoSim);
    }
    return filteredItems;
  }
}
