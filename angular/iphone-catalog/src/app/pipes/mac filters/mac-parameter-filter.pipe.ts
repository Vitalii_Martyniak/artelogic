import { Pipe, PipeTransform } from '@angular/core';
import {IMacProduct} from '../../interfaces/macProduct';
import {macColor, macDiagonal, macHasTouchBar, macMemory, macModel} from '../../parameters/macParameters';
import {HelpingService} from '../../services/helping.service';

@Pipe({
  name: 'macParameterFilter'
})
export class MacParameterFilterPipe implements PipeTransform {
  constructor(public helpingService: HelpingService) {}
  transform(
    items: IMacProduct[],
    modelsToFilter: string[],
    diagonalsToFilter: string[],
    colorsToFilter: string[],
    memoriesToFilter: string[],
    hasTouchBarsToFilter: string[]): IMacProduct[] {
    let filteredItems = items;
    if (modelsToFilter.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, modelsToFilter, macModel);
    }
    if (diagonalsToFilter.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, diagonalsToFilter, macDiagonal);
    }
    if (colorsToFilter.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, colorsToFilter, macColor);
    }
    if (memoriesToFilter.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, memoriesToFilter, macMemory);
    }
    if (hasTouchBarsToFilter.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, hasTouchBarsToFilter, macHasTouchBar);
    }
    return filteredItems;
  }
}
