import { Pipe, PipeTransform } from '@angular/core';
import {IWatchProduct} from '../../interfaces/watchProduct';
import {watchColor, watchGeneration, watchModel, watchSize, watchStrapColor, watchStrapSeries} from '../../parameters/watchParameters';
import {HelpingService} from '../../services/helping.service';

@Pipe({
  name: 'watchParameterFilter'
})
export class WatchParameterFilterPipe implements PipeTransform {
  constructor(public helpingService: HelpingService) {}
  transform(
    items: IWatchProduct[],
    generationsToFilter: number[],
    modelsToFilter: string[],
    sizesToFilter: number[],
    colorsToFilter: string[],
    strapSeriesToFilter: string[],
    strapColorsToFilter: string[]): IWatchProduct[] {
    let filteredItems = items;
    if (generationsToFilter.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, generationsToFilter, watchGeneration);
    }
    if (modelsToFilter.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, modelsToFilter, watchModel);
    }
    if (sizesToFilter.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, sizesToFilter, watchSize);
    }
    if (colorsToFilter.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, colorsToFilter, watchColor);
    }
    if (strapSeriesToFilter.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, strapSeriesToFilter, watchStrapSeries);
    }
    if (strapColorsToFilter.length) {
      filteredItems = this.helpingService.getFilteredItemsBy(filteredItems, strapColorsToFilter, watchStrapColor);
    }
    return filteredItems;
  }
}
