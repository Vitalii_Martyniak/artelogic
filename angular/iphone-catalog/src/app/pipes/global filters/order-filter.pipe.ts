import { Pipe, PipeTransform } from '@angular/core';
import {IWatchProduct} from '../../interfaces/watchProduct';
import {IMacProduct} from '../../interfaces/macProduct';
import {IIphoneProduct} from '../../interfaces/iphoneProduct';

@Pipe({
  name: 'orderFilter'
})
export class OrderFilterPipe implements PipeTransform {
  transform(products: IIphoneProduct[] | IMacProduct[] | IWatchProduct[],
            order: string): IIphoneProduct[] | IMacProduct[] | IWatchProduct[] {
    if (order === 'asc') {
      return products.sort((a, b) => a.price - b.price);
    }
    return products.sort((a, b) => b.price - a.price);
  }
}
