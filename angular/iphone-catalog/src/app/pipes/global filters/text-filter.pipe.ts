import { Pipe, PipeTransform } from '@angular/core';
import {IWatchProduct} from '../../interfaces/watchProduct';
import {IMacProduct} from '../../interfaces/macProduct';
import {IIphoneProduct} from '../../interfaces/iphoneProduct';

@Pipe({
  name: 'textFilter'
})
export class TextFilterPipe implements PipeTransform {
  transform(products: IIphoneProduct[] | IMacProduct[] | IWatchProduct[],
            textFilter: string): IIphoneProduct[] | IMacProduct[] | IWatchProduct[] {
    if (!textFilter) {
      return products;
    }

    const filteredArray = [];
    textFilter = textFilter.toLowerCase();

    products.forEach(item => {
      let stringFromItem = '';
      for (const key in item) {
        stringFromItem += item[key].toString().toLowerCase() + ' ';
      }
      if (stringFromItem.includes(textFilter)) {
        filteredArray.push(item);
      }
    });
    return filteredArray;
  }
}
