import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'priceFilter'
})
export class PriceFilterPipe implements PipeTransform {
  transform(products, minPrice: number, maxPrice: number) {
    return products.filter(product => product.price >= minPrice && product.price <= maxPrice);
  }
}
