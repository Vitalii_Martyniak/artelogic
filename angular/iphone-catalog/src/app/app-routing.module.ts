import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LayoutComponent} from './components/layout/layout.component';
import {IphoneCatalogComponent} from './components/iphone-catalog/iphone-catalog.component';
import {IphoneCatalogItemComponent} from './components/iphone-catalog-item/iphone-catalog-item.component';
import {CartComponent} from './components/cart/cart.component';
import {MacCatalogComponent} from './components/mac-catalog/mac-catalog.component';
import {AppleWatchCatalogComponent} from './components/apple-watch-catalog/apple-watch-catalog.component';
import {MacCatalogItemComponent} from './components/mac-catalog-item/mac-catalog-item.component';
import {AppleWatchCatalogItemComponent} from './components/apple-watch-catalog-item/apple-watch-catalog-item.component';
import {FeedbackPageComponent} from './components/feedback-page/feedback-page.component';


const routes: Routes = [
  {
    path: '', component: LayoutComponent, children: [
      {path: '', redirectTo: 'iphone', pathMatch: 'full'},
      {path: 'iphone', component: IphoneCatalogComponent},
      {path: 'iphone/:id', component: IphoneCatalogItemComponent},
      {path: 'mac', component: MacCatalogComponent},
      {path: 'mac/:id', component: MacCatalogItemComponent},
      {path: 'watch', component: AppleWatchCatalogComponent},
      {path: 'watch/:id', component: AppleWatchCatalogItemComponent},
      {path: 'cart', component: CartComponent},
      {path: 'feedback', component: FeedbackPageComponent},
      {path: '**', redirectTo: '/'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
