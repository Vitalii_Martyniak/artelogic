export interface IMacProduct {
  model: string;
  diagonal: string;
  memory: string;
  color: string;
  hasTouchBar: string;
  price: number;
  photo: string;
  countInCart: number;
  id: string;
}
