export interface IFeedback {
  userName: string;
  userFeedback: string;
  date: object;
}
