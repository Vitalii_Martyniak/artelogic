export interface IIphoneProduct {
  model: string;
  memory: number;
  color: string;
  hasTwoSim: string;
  condition: string;
  price: number;
  photo: string;
  countInCart?: number;
  id?: string;
}
