export interface IWatchProduct {
  generation: number;
  model: string;
  size: number;
  color: string;
  strapSeries: string;
  strapColor: string;
  price: number;
  photo: string;
  countInCart: number;
  id: string;
}
