export interface ICategory {
  name: string;
  checked: boolean;
}
