import {Injectable} from '@angular/core';
import {ICategory} from '../interfaces/category';

@Injectable({
  providedIn: 'root'
})
export class IphoneFiltersService {
  modelCategories: ICategory[] = [];
  memoryCategories: ICategory[] = [];
  conditionCategories: ICategory[] = [];
  colorCategories: ICategory[] = [];
  hasTwoSimCategories: ICategory[] = [];
  modelsToFilter: string[] = [];
  memoriesToFilter: number[] = [];
  conditionsToFilter: string[] = [];
  colorsToFilter: string[] = [];
  hasTwoSimsToFilter: string[] = [];

  minPrice = 0;
  maxPrice = null;
  ceil: number;

  currentPage = 1;
  itemsPerPage = 9;
  itemsOrder = '';

  constructor() { }
}
