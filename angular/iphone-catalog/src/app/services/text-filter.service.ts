import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TextFilterService {

  public textFilter$ = new Subject<string>();
  public textClearer$ = new Subject<string>();
  constructor() { }

  setTextFilter(value: string) {
    this.textFilter$.next(value);
  }

  clearTextFilter() {
    this.textClearer$.next();
  }
}
