import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlertService {
  public alert$ = new Subject<string>();

  alert(text: string) {
    this.alert$.next(text);
  }
}
