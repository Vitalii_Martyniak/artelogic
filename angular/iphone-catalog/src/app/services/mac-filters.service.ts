import { Injectable } from '@angular/core';
import {ICategory} from '../interfaces/category';

@Injectable({
  providedIn: 'root'
})
export class MacFiltersService {
  modelCategories: ICategory[] = [];
  diagonalCategories: ICategory[] = [];
  memoryCategories: ICategory[] = [];
  colorCategories: ICategory[] = [];
  hasTouchBarCategories: ICategory[] = [];
  modelsToFilter: string[] = [];
  diagonalsToFilter: string[] = [];
  memoriesToFilter: string[] = [];
  colorsToFilter: string[] = [];
  hasTouchBarsToFilter: string[] = [];

  minPrice = 0;
  maxPrice = null;
  ceil: number;

  currentPage = 1;
  itemsPerPage = 9;
  itemsOrder = '';

  constructor() { }
}
