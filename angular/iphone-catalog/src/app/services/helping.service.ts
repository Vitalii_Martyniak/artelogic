import { Injectable } from '@angular/core';
import {ICategory} from '../interfaces/category';
import {IIphoneProduct} from '../interfaces/iphoneProduct';
import {IMacProduct} from '../interfaces/macProduct';
import {IWatchProduct} from '../interfaces/watchProduct';
import {watchColor, watchGeneration, watchModel, watchSize, watchStrapColor, watchStrapSeries} from '../parameters/watchParameters';
import {iPhoneColor, iPhoneMemory} from '../parameters/iphoneParameters';
import {macColor, macModel} from '../parameters/macParameters';

@Injectable({
  providedIn: 'root'
})
export class HelpingService {

  constructor() { }

  changeCategoryValues($event, category: ICategory, categoryArray: ICategory[]) {
    categoryArray.forEach(item => {
      if (item.name === category.name) {
        item.checked = $event.target.checked;
      }
    });
  }

  getFilteredItemsBy(items: IIphoneProduct[] | IMacProduct[] | IWatchProduct[],
                     filters: number[]|string[], parameter: string): any[] {
    const filteredItems = [];

    items.forEach(item => {
      filters.forEach(filterItem => {
        if (item[parameter] === filterItem) {
          filteredItems.push(item);
        }
      });
    });
    return filteredItems;
  }

  formCategory(products: IIphoneProduct[] | IMacProduct[] | IWatchProduct[], parameter: string): ICategory[] {
    let parameterArray = [];

    products.forEach(item => {
      parameterArray.push(item[parameter]);
    });

    parameterArray = [...new Set(parameterArray)];

    if (isIphoneArray(products[0])) {
      if (parameter === iPhoneColor) {
        parameterArray.sort();
      }
      if (parameter === iPhoneMemory) {
        parameterArray.sort((a, b) => a - b);
      }
    }
    if (isMacArray(products[0])) {
      if (parameter === macColor) {
        parameterArray.sort();
      }
    }
    if (isWatchArray(products[0])) {
      if (parameter === watchModel || watchColor || watchStrapSeries || watchStrapColor) {
        parameterArray.sort();
      }
      if (parameter === watchSize) {
        parameterArray.sort((a, b) => a - b);
      }
    }

    const categoryArray = [];
    parameterArray.forEach(item => {
      categoryArray.push({name: item, checked: false});
    });
    return categoryArray;
  }

  changeFilter($event, value: string, filterArray: string[]) {
    if ($event.target.checked) {
      filterArray = [...filterArray, value];
    } else {
      const valueIndex = filterArray.indexOf(value);
      filterArray.splice(valueIndex, 1);
      filterArray = [...filterArray];
    }
    return filterArray;
  }
}

const isIphoneArray = (variableToCheck: any): variableToCheck is IIphoneProduct[] =>
  (variableToCheck as IIphoneProduct).hasTwoSim !== undefined;

const isMacArray = (variableToCheck: any): variableToCheck is IMacProduct[] =>
  (variableToCheck as IMacProduct).hasTouchBar !== undefined;

const isWatchArray = (variableToCheck: any): variableToCheck is IWatchProduct[] =>
  (variableToCheck as IWatchProduct).generation !== undefined;
