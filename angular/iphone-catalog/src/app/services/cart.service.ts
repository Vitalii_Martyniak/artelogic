import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {IIphoneProduct} from '../interfaces/iphoneProduct';
import {IMacProduct} from '../interfaces/macProduct';
import {IWatchProduct} from '../interfaces/watchProduct';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  cartProducts: IIphoneProduct[] | IMacProduct[] | IWatchProduct[] = [];

  public cartCounter$ = new Subject<any>();

  constructor() { }

  changeCartCounter() {
    let counter = 0;
    this.cartProducts.forEach(item => {
      counter += item.countInCart;
    });
    this.cartCounter$.next(counter);
  }

  addItemToCart(product) {
    for (const item of this.cartProducts) {
      if (item.id === product.id) {
        item.countInCart++;
        this.changeCartCounter();
        return;
      }
    }
    this.cartProducts.push(product);
    this.changeCartCounter();
  }
}
