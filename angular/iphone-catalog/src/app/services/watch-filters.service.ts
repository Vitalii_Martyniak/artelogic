import { Injectable } from '@angular/core';
import {ICategory} from '../interfaces/category';

@Injectable({
  providedIn: 'root'
})
export class WatchFiltersService {
  generationCategories: ICategory[] = [];
  modelCategories: ICategory[] = [];
  sizeCategories: ICategory[] = [];
  colorCategories: ICategory[] = [];
  strapSeriesCategories: ICategory[] = [];
  strapColorCategories: ICategory[] = [];
  generationsToFilter: number[] = [];
  modelsToFilter: string[] = [];
  sizesToFilter: number[] = [];
  colorsToFilter: string[] = [];
  strapSeriesToFilter: string[] = [];
  strapColorsToFilter: string[] = [];

  minPrice = 0;
  maxPrice = null;
  ceil: number;

  currentPage = 1;
  itemsPerPage = 9;
  itemsOrder = '';

  constructor() { }
}
