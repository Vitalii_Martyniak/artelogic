import { Injectable } from '@angular/core';
import data from './products.json';
import {IIphoneProduct} from '../../interfaces/iphoneProduct';
import {IMacProduct} from '../../interfaces/macProduct';
import {IWatchProduct} from '../../interfaces/watchProduct';

@Injectable({
  providedIn: 'root'
})

export class StoreService {
  iPhones: IIphoneProduct[] = data.iPhones;
  macs: IMacProduct[] = data.macs;
  watches: IWatchProduct[] = data.watches;

  constructor() {}

  getIPhoneById(id: string): IIphoneProduct {
    return this.iPhones.find(item => item.id === id);
  }

  getMacById(id: string): IMacProduct {
    return this.macs.find(item => item.id === id);
  }

  getWatchById(id: string): IWatchProduct {
    return this.watches.find(item => item.id === id);
  }
}
