import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {CartService} from '../../services/cart.service';
import {TextFilterService} from '../../services/text-filter.service';


@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit, OnDestroy {
  @ViewChild('textFilter', {static: false}) textFilter: ElementRef;
  cartCounter = 0;
  constructor(
    public cartService: CartService,
    public textFilterService: TextFilterService) { }

  ngOnInit() {
    this.cartService.cartCounter$
      .subscribe((value: number) => this.cartCounter = value);
    this.textFilterService.textClearer$.subscribe(() => this.textFilter.nativeElement.value = '');
  }

  ngOnDestroy() {
    this.cartService.cartCounter$.unsubscribe();
  }

  sendText(value: string) {
    this.textFilterService.setTextFilter(value);
  }
}
