import { Component, OnInit } from '@angular/core';
import {StoreService} from '../../services/store/store.service';
import {ActivatedRoute, Params} from '@angular/router';
import {AlertService} from '../../services/alert.service';
import {CartService} from '../../services/cart.service';

@Component({
  selector: 'app-mac-catalog-item',
  templateUrl: './mac-catalog-item.component.html',
  styleUrls: ['./mac-catalog-item.component.scss']
})
export class MacCatalogItemComponent implements OnInit {
  product;

  constructor(
    public storeService: StoreService,
    public route: ActivatedRoute,
    public alertService: AlertService,
    public cartService: CartService) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.product = this.storeService.getMacById(params.id);
    });
  }

  addItem(product) {
    this.cartService.addItemToCart(product);
    this.alertService.alert('Product added to cart');
  }
}
