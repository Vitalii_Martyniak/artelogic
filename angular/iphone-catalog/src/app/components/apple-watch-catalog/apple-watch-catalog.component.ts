import {Component, OnDestroy, OnInit} from '@angular/core';
import {Options} from 'ng5-slider';
import {ICategory} from '../../interfaces/category';
import {StoreService} from '../../services/store/store.service';
import {AlertService} from '../../services/alert.service';
import {CartService} from '../../services/cart.service';
import {TextFilterService} from '../../services/text-filter.service';
import {IWatchProduct} from '../../interfaces/watchProduct';
import {WatchFiltersService} from '../../services/watch-filters.service';
import {watchColor, watchGeneration, watchModel, watchSize, watchStrapColor, watchStrapSeries} from '../../parameters/watchParameters';
import {HelpingService} from '../../services/helping.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-apple-watch-catalog',
  templateUrl: './apple-watch-catalog.component.html',
  styleUrls: ['./apple-watch-catalog.component.scss']
})
export class AppleWatchCatalogComponent implements OnInit, OnDestroy {
  products: IWatchProduct[];
  minPrice = this.watchFiltersService.minPrice;
  maxPrice = this.watchFiltersService.maxPrice;
  options: Options = {
    floor: 0,
    ceil: this.watchFiltersService.ceil,
    step: 5
  };
  generationCategories: ICategory[] = this.watchFiltersService.generationCategories;
  modelCategories: ICategory[] = this.watchFiltersService.modelCategories;
  sizeCategories: ICategory[] = this.watchFiltersService.sizeCategories;
  colorCategories: ICategory[] = this.watchFiltersService.colorCategories;
  strapSeriesCategories: ICategory[] = this.watchFiltersService.strapSeriesCategories;
  strapColorCategories: ICategory[] = this.watchFiltersService.strapColorCategories;
  generationsToFilter: number[] = this.watchFiltersService.generationsToFilter;
  modelsToFilter: string[] = this.watchFiltersService.modelsToFilter;
  sizesToFilter: number[] = this.watchFiltersService.sizesToFilter;
  colorsToFilter: string[] = this.watchFiltersService.colorsToFilter;
  strapSeriesToFilter: string[] = this.watchFiltersService.strapSeriesToFilter;
  strapColorsToFilter: string[] = this.watchFiltersService.strapColorsToFilter;

  itemsOrder = this.watchFiltersService.itemsOrder;
  textFilter: string;
  currentPage = this.watchFiltersService.currentPage;
  itemsPerPage = this.watchFiltersService.itemsPerPage;
  sub: Subscription;

  constructor(
    public storeService: StoreService,
    public alertService: AlertService,
    public watchFiltersService: WatchFiltersService,
    public cartService: CartService,
    public textFilterService: TextFilterService,
    public helpingService: HelpingService) { }

  ngOnInit() {
    this.sub = this.textFilterService.textFilter$.subscribe((value: string) => this.textFilter = value);
    this.products = this.storeService.watches;

    if (!this.maxPrice) {
      this.products.forEach(product => {
        if (product.price > this.maxPrice) {
          this.maxPrice = this.options.ceil =  this.watchFiltersService.ceil = product.price;
        }
      });
    }

    if (!this.modelCategories.length) {
      this.watchFiltersService.generationCategories = this.generationCategories =
        this.helpingService.formCategory(this.storeService.watches, watchGeneration);
      this.watchFiltersService.modelCategories = this.modelCategories =
        this.helpingService.formCategory(this.storeService.watches, watchModel);
      this.watchFiltersService.sizeCategories = this.sizeCategories =
        this.helpingService.formCategory(this.storeService.watches, watchSize);
      this.watchFiltersService.colorCategories = this.colorCategories =
        this.helpingService.formCategory(this.storeService.watches, watchColor);
      this.watchFiltersService.strapSeriesCategories = this.strapSeriesCategories =
        this.helpingService.formCategory(this.storeService.watches, watchStrapSeries);
      this.watchFiltersService.strapColorCategories = this.strapColorCategories =
        this.helpingService.formCategory(this.storeService.watches, watchStrapColor);
    }
  }

  setChanges($event, category: ICategory, filtersArray: string, categoriesArray: string) {
    this.watchFiltersService[filtersArray] = this[filtersArray] =
      this.helpingService.changeFilter($event, category.name, this[filtersArray]);

    this.helpingService.changeCategoryValues($event, category, this.watchFiltersService[categoriesArray]);
    this.changeCurrentPage(1);
  }

  setOrder(value: string) {
    this.itemsOrder = this.watchFiltersService.itemsOrder = value;
  }

  setItemsPerPage(value: number) {
    this.itemsPerPage = this.watchFiltersService.itemsPerPage = value;
  }

  addItem(product: IWatchProduct) {
    this.cartService.addItemToCart(product);
    this.alertService.alert('Product added to cart');
  }

  priceOnChange(minPrice: number, maxPrice: number) {
    this.watchFiltersService.minPrice = minPrice;
    this.watchFiltersService.maxPrice = maxPrice;
  }

  changeCurrentPage($event: number) {
    this.currentPage = this.watchFiltersService.currentPage = $event;
  }

  ngOnDestroy() {
    this.textFilterService.clearTextFilter();
    this.sub.unsubscribe();
  }
}

