import {Component, OnInit} from '@angular/core';
import {IFeedback} from '../../interfaces/feedback';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-feedback-page',
  templateUrl: './feedback-page.component.html',
  styleUrls: ['./feedback-page.component.scss']
})
export class FeedbackPageComponent implements OnInit {
  form: FormGroup;
  feedbacks: IFeedback[];
  constructor() { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      feedback: new FormControl('', Validators.required)
    });

    this.feedbacks = JSON.parse(localStorage.getItem('feedbacks')) || [];
  }

  addFeedback() {
    const newFeedback = {userName: this.form.value.name,
      userFeedback: this.form.value.feedback, date: new Date()};

    this.feedbacks.unshift(newFeedback);
    localStorage.setItem('feedbacks', JSON.stringify(this.feedbacks));

    this.form.value.name = '';
    this.form.value.feedback = '';
  }
}
