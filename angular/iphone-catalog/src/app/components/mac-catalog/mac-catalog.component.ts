import {Component, OnDestroy, OnInit} from '@angular/core';
import {Options} from 'ng5-slider';
import {ICategory} from '../../interfaces/category';
import {StoreService} from '../../services/store/store.service';
import {AlertService} from '../../services/alert.service';
import {CartService} from '../../services/cart.service';
import {IMacProduct} from '../../interfaces/macProduct';
import {MacFiltersService} from '../../services/mac-filters.service';
import {macColor, macDiagonal, macHasTouchBar, macMemory, macModel} from '../../parameters/macParameters';
import {TextFilterService} from '../../services/text-filter.service';
import {HelpingService} from '../../services/helping.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-mac-catalog',
  templateUrl: './mac-catalog.component.html',
  styleUrls: ['./mac-catalog.component.scss']
})
export class MacCatalogComponent implements OnInit, OnDestroy {
  products: IMacProduct[];
  minPrice = this.macFiltersService.minPrice;
  maxPrice = this.macFiltersService.maxPrice;
  options: Options = {
    floor: 0,
    ceil: this.macFiltersService.ceil,
    step: 5
  };
  modelCategories: ICategory[] = this.macFiltersService.modelCategories;
  memoryCategories: ICategory[] = this.macFiltersService.memoryCategories;
  diagonalCategories: ICategory[] = this.macFiltersService.diagonalCategories;
  colorCategories: ICategory[] = this.macFiltersService.colorCategories;
  hasTouchBarCategories: ICategory[] = this.macFiltersService.hasTouchBarCategories;
  modelsToFilter: string[] = this.macFiltersService.modelsToFilter;
  diagonalsToFilter: string[] = this.macFiltersService.diagonalsToFilter;
  memoriesToFilter: string[] = this.macFiltersService.memoriesToFilter;
  colorsToFilter: string[] = this.macFiltersService.colorsToFilter;
  hasTouchBarsToFilter: string[] = this.macFiltersService.hasTouchBarsToFilter;

  itemsOrder = this.macFiltersService.itemsOrder;
  textFilter: string;
  currentPage = this.macFiltersService.currentPage;
  itemsPerPage = this.macFiltersService.itemsPerPage;
  sub: Subscription;

  constructor(
    public storeService: StoreService,
    public alertService: AlertService,
    public macFiltersService: MacFiltersService,
    public cartService: CartService,
    public textFilterService: TextFilterService,
    public helpingService: HelpingService) { }

   ngOnInit() {
     this.sub = this.textFilterService.textFilter$.subscribe((value: string) => this.textFilter = value);
     this.products = this.storeService.macs;

     if (!this.maxPrice) {
       this.products.forEach(product => {
         if (product.price > this.maxPrice) {
           this.maxPrice = this.options.ceil =  this.macFiltersService.ceil = product.price;
         }
       });
     }

     if (!this.modelCategories.length) {
       this.macFiltersService.modelCategories = this.modelCategories =
         this.helpingService.formCategory(this.storeService.macs, macModel);
       this.macFiltersService.memoryCategories = this.memoryCategories =
         this.helpingService.formCategory(this.storeService.macs, macMemory);
       this.macFiltersService.diagonalCategories = this.diagonalCategories =
         this.helpingService.formCategory(this.storeService.macs, macDiagonal);
       this.macFiltersService.colorCategories = this.colorCategories =
         this.helpingService.formCategory(this.storeService.macs, macColor);
       this.macFiltersService.hasTouchBarCategories = this.hasTouchBarCategories =
         this.helpingService.formCategory(this.storeService.macs, macHasTouchBar);
     }
   }

   setChanges($event, category: ICategory, filtersArray: string, categoriesArray: string) {
     this.macFiltersService[filtersArray] = this[filtersArray] =
       this.helpingService.changeFilter($event, category.name, this[filtersArray]);

     this.helpingService.changeCategoryValues($event, category, this.macFiltersService[categoriesArray]);
     this.changeCurrentPage(1);
   }

   setOrder(value: string) {
     this.itemsOrder = this.macFiltersService.itemsOrder = value;
   }

   setItemsPerPage(value: number) {
     this.itemsPerPage = this.macFiltersService.itemsPerPage = value;
   }

   addItem(product: IMacProduct) {
     this.cartService.addItemToCart(product);
     this.alertService.alert('Product added to cart');
   }

   priceOnChange(minPrice: number, maxPrice: number) {
     this.macFiltersService.minPrice = minPrice;
     this.macFiltersService.maxPrice = maxPrice;
   }

   changeCurrentPage($event: number) {
     this.currentPage = this.macFiltersService.currentPage = $event;
   }

   ngOnDestroy() {
     this.textFilterService.clearTextFilter();
     this.sub.unsubscribe();
   }
}
