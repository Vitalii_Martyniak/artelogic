import { Component, OnInit } from '@angular/core';
import {StoreService} from '../../services/store/store.service';
import {ActivatedRoute, Params} from '@angular/router';
import {AlertService} from '../../services/alert.service';
import {CartService} from '../../services/cart.service';
import {IWatchProduct} from '../../interfaces/watchProduct';

@Component({
  selector: 'app-apple-watch-catalog-item',
  templateUrl: './apple-watch-catalog-item.component.html',
  styleUrls: ['./apple-watch-catalog-item.component.scss']
})
export class AppleWatchCatalogItemComponent implements OnInit {
  product: IWatchProduct;

  constructor(
    public storeService: StoreService,
    public route: ActivatedRoute,
    public alertService: AlertService,
    public cartService: CartService) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.product = this.storeService.getWatchById(params.id);
    });
  }

  addItem(product) {
    this.cartService.addItemToCart(product);
    this.alertService.alert('Product added to cart');
  }
}
