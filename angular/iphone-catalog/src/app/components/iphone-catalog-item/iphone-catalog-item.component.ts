import {Component, OnInit} from '@angular/core';
import {StoreService} from '../../services/store/store.service';
import {ActivatedRoute, Params} from '@angular/router';
import {AlertService} from '../../services/alert.service';
import {CartService} from '../../services/cart.service';
import {IIphoneProduct} from '../../interfaces/iphoneProduct';

@Component({
  selector: 'app-catalog-item',
  templateUrl: './iphone-catalog-item.component.html',
  styleUrls: ['./iphone-catalog-item.component.scss']
})
export class IphoneCatalogItemComponent implements OnInit {
  product: IIphoneProduct;

  constructor(
    public storeService: StoreService,
    public route: ActivatedRoute,
    public alertService: AlertService,
    public cartService: CartService) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.product = this.storeService.getIPhoneById(params.id);
    });
  }

  addItem(product) {
    this.cartService.addItemToCart(product);
    this.alertService.alert('Product added to cart');
  }
}
