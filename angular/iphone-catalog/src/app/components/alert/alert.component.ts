import {Component, OnInit} from '@angular/core';
import {AlertService} from '../../services/alert.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  animations: [
    trigger('alert', [
      state('void', style({right: '-200px'})),
      state('*', style({right: '25px'})),
      transition('void => *', animate(200)),
      transition('* => void', animate(200))
    ])
  ]
})
export class AlertComponent implements OnInit {
  text: string;

  constructor(public alertService: AlertService) { }

  ngOnInit() {
    this.alertService.alert$.subscribe(text => {
      this.text = text;
      const timeout = setTimeout(() => {
        clearTimeout(timeout);
        this.text = '';
      }, 2000);
    });
  }
}
