import { CartComponent } from './cart.component';
import {AlertService} from '../../services/alert.service';
import {CartService} from '../../services/cart.service';

describe('TestComponent', () => {
  let alertService;
  let cartService;
  let component;

  beforeEach(() => {
    alertService = new AlertService();
    cartService = new CartService();
    component = new CartComponent(alertService, cartService);
    component.cartProducts = [
      {
        generation: 3,
        model: 'Apple Watch Sport',
        size: 38,
        color: 'Space Gray',
        strapSeries: 'Sport Band',
        strapColor: 'Black',
        price: 289,
        photo: 'sport-38-space',
        countInCart: 3,
        id: '78'
      },
      {
        generation: 3,
        model: 'Apple Watch Sport',
        size: 38,
        color: 'Silver',
        strapSeries: 'Sport Band',
        strapColor: 'White',
        price: 289,
        photo: 'sport-38-silver',
        countInCart: 4,
        id: '79'
      }];
  });

  it('should update cartProducts from service when ngOnInit', () => {
    component.cartProducts = [];
    component.ngOnInit();
    expect(component.cartProducts).toBe(cartService.cartProducts);
  });

  it('should call calculateTotal() when ngOnInit', () => {
    const spy = spyOn(component, 'calculateTotal');
    component.ngOnInit();
    expect(spy).toHaveBeenCalled();
  });

  it('should delete item from cart', () => {
    component.deleteFromCart(component.cartProducts[0], 0);
    expect(component.cartProducts).toEqual(component.cartProducts);
  });

  it('should call calculateTotal() after item delete', () => {
    const spy = spyOn(component, 'calculateTotal');
    component.deleteFromCart(component.cartProducts[0], 0);
    expect(spy).toHaveBeenCalled();
  });

  it('should call changeCartCounter() in cartService after deleting item', () => {
    const spy = spyOn(cartService, 'changeCartCounter');
    component.deleteFromCart(component.cartProducts[0], 0);
    expect(spy).toHaveBeenCalled();
  });

  it('should calculate total', () => {
    component.total = 0;
    component.calculateTotal();
    expect(component.total).toBe(2023);
  });

  it('should refresh cart', () => {
    component.cartService.cartProducts = component.cartProducts;
    component.total = 578;
    component.submitOrder();
    expect(component.cartProducts).toEqual([]);
    expect(component.cartService.cartProducts).toEqual([]);
    expect(component.total).toBe(0);
  });
});
