import { Component, OnInit } from '@angular/core';
import {AlertService} from '../../services/alert.service';
import {CartService} from '../../services/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  cartProducts: any[];
  total = 0;

  constructor(
    public alertService: AlertService,
    public cartService: CartService) { }

  ngOnInit() {
    this.cartProducts = this.cartService.cartProducts;
    this.calculateTotal();
  }

  deleteFromCart(product, index: number) {
    product.countInCart = 1;

    this.cartProducts.splice(index, 1);
    this.calculateTotal();
    this.cartService.changeCartCounter();
  }

  calculateTotal() {
    this.total = this.cartProducts.reduce((sum, current) => {
      return sum += current.price * current.countInCart;
    }, 0);
  }

  submitOrder() {
    this.cartProducts = [];
    this.cartService.cartProducts = [];
    this.total = 0;

    this.cartService.changeCartCounter();
    this.alertService.alert('Thank`s for your order!');
  }
}
