import {Component, OnDestroy, OnInit} from '@angular/core';
import {StoreService} from '../../services/store/store.service';
import {Options} from 'ng5-slider';
import {AlertService} from '../../services/alert.service';
import {IphoneFiltersService} from '../../services/iphone-filters.service';
import {iPhoneColor, iPhoneCondition, iPhoneHasTwoSim, iPhoneMemory, iPhoneModel} from '../../parameters/iphoneParameters';
import {IIphoneProduct} from '../../interfaces/iphoneProduct';
import {ICategory} from '../../interfaces/category';
import {CartService} from '../../services/cart.service';
import {TextFilterService} from '../../services/text-filter.service';
import {HelpingService} from '../../services/helping.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-catalog',
  templateUrl: './iphone-catalog.component.html',
  styleUrls: ['./iphone-catalog.component.scss']
})
export class IphoneCatalogComponent implements OnInit, OnDestroy {
  products: IIphoneProduct[];
  minPrice = this.iphoneFiltersService.minPrice;
  maxPrice = this.iphoneFiltersService.maxPrice;
  options: Options = {
    floor: 0,
    ceil: this.iphoneFiltersService.ceil,
    step: 5
  };
  modelCategories: ICategory[] = this.iphoneFiltersService.modelCategories;
  memoryCategories: ICategory[] = this.iphoneFiltersService.memoryCategories;
  conditionCategories: ICategory[] = this.iphoneFiltersService.conditionCategories;
  colorCategories: ICategory[] = this.iphoneFiltersService.colorCategories;
  hasTwoSimCategories: ICategory[] = this.iphoneFiltersService.hasTwoSimCategories;
  modelsToFilter: string[] = this.iphoneFiltersService.modelsToFilter;
  memoriesToFilter: number[] = this.iphoneFiltersService.memoriesToFilter;
  conditionsToFilter: string[] = this.iphoneFiltersService.conditionsToFilter;
  colorsToFilter: string[] = this.iphoneFiltersService.colorsToFilter;
  hasTwoSimsToFilter: string[] = this.iphoneFiltersService.hasTwoSimsToFilter;

  itemsOrder = this.iphoneFiltersService.itemsOrder;
  textFilter: string;
  currentPage = this.iphoneFiltersService.currentPage;
  itemsPerPage = this.iphoneFiltersService.itemsPerPage;
  sub: Subscription;

  constructor(
    public storeService: StoreService,
    public alertService: AlertService,
    public iphoneFiltersService: IphoneFiltersService,
    public cartService: CartService,
    public textFilterService: TextFilterService,
    public helpingService: HelpingService) { }

  ngOnInit() {
    this.sub = this.textFilterService.textFilter$.subscribe((value: string) => this.textFilter = value);
    this.products = this.storeService.iPhones;

    if (!this.maxPrice) {
      this.products.forEach(product => {
        if (product.price > this.maxPrice) {
          this.maxPrice = this.options.ceil =  this.iphoneFiltersService.ceil = product.price;
        }
      });
    }

    if (!this.modelCategories.length) {
      this.iphoneFiltersService.modelCategories = this.modelCategories =
        this.helpingService.formCategory(this.storeService.iPhones, iPhoneModel);
      this.iphoneFiltersService.memoryCategories = this.memoryCategories =
        this.helpingService.formCategory(this.storeService.iPhones, iPhoneMemory);
      this.iphoneFiltersService.conditionCategories = this.conditionCategories =
        this.helpingService.formCategory(this.storeService.iPhones, iPhoneCondition);
      this.iphoneFiltersService.colorCategories = this.colorCategories =
        this.helpingService.formCategory(this.storeService.iPhones, iPhoneColor);
      this.iphoneFiltersService.hasTwoSimCategories = this.hasTwoSimCategories =
        this.helpingService.formCategory(this.storeService.iPhones, iPhoneHasTwoSim);
    }
  }

  setChanges($event, category: ICategory, filtersArray: string, categoriesArray: string) {
    this.iphoneFiltersService[filtersArray] = this[filtersArray] =
      this.helpingService.changeFilter($event, category.name, this[filtersArray]);

    this.helpingService.changeCategoryValues($event, category, this.iphoneFiltersService[categoriesArray]);
    this.changeCurrentPage(1);
  }

  setOrder(value: string) {
    this.itemsOrder = this.iphoneFiltersService.itemsOrder = value;
  }

  setItemsPerPage(value: number) {
    this.itemsPerPage = this.iphoneFiltersService.itemsPerPage = value;
  }

  addItem(product: IIphoneProduct) {
    this.cartService.addItemToCart(product);
    this.alertService.alert('Product added to cart');
  }

  priceOnChange(minPrice: number, maxPrice: number) {
    this.iphoneFiltersService.minPrice = minPrice;
    this.iphoneFiltersService.maxPrice = maxPrice;
  }

  changeCurrentPage($event: number) {
    this.currentPage = this.iphoneFiltersService.currentPage = $event;
  }

  ngOnDestroy() {
    this.textFilterService.clearTextFilter();
    this.sub.unsubscribe();
  }
}
