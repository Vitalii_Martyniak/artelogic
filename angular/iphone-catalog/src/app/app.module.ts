import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './components/layout/layout.component';
import { IphoneCatalogComponent } from './components/iphone-catalog/iphone-catalog.component';
import { IphoneCatalogItemComponent } from './components/iphone-catalog-item/iphone-catalog-item.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CartComponent } from './components/cart/cart.component';
import {Ng5SliderModule} from 'ng5-slider';
import { IphoneParameterFilterPipe } from './pipes/iphone filters/iphone-parameter-filter.pipe';
import { NgxPaginationModule } from 'ngx-pagination';
import { AlertComponent } from './components/alert/alert.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MacCatalogComponent} from './components/mac-catalog/mac-catalog.component';
import { AppleWatchCatalogComponent } from './components/apple-watch-catalog/apple-watch-catalog.component';
import { MacParameterFilterPipe } from './pipes/mac filters/mac-parameter-filter.pipe';
import { MacCatalogItemComponent } from './components/mac-catalog-item/mac-catalog-item.component';
import { OrderFilterPipe } from './pipes/global filters/order-filter.pipe';
import { WatchParameterFilterPipe } from './pipes/watch filters/watch-parameter-filter.pipe';
import { PriceFilterPipe } from './pipes/global filters/price-filter.pipe';
import { TextFilterPipe } from './pipes/global filters/text-filter.pipe';
import { AppleWatchCatalogItemComponent } from './components/apple-watch-catalog-item/apple-watch-catalog-item.component';
import { FeedbackPageComponent } from './components/feedback-page/feedback-page.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    IphoneCatalogComponent,
    IphoneCatalogItemComponent,
    CartComponent,
    IphoneParameterFilterPipe,
    AlertComponent,
    MacCatalogComponent,
    AppleWatchCatalogComponent,
    MacParameterFilterPipe,
    MacCatalogItemComponent,
    OrderFilterPipe,
    WatchParameterFilterPipe,
    PriceFilterPipe,
    TextFilterPipe,
    AppleWatchCatalogItemComponent,
    FeedbackPageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    Ng5SliderModule,
    NgxPaginationModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
