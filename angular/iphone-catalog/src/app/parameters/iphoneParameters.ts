export const iPhoneModel = 'model';
export const iPhoneCondition = 'condition';
export const iPhoneColor = 'color';
export const iPhoneMemory = 'memory';
export const iPhoneHasTwoSim = 'hasTwoSim';
