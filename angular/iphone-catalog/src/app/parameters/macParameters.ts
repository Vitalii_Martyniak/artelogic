export const macModel = 'model';
export const macDiagonal = 'diagonal';
export const macColor = 'color';
export const macMemory = 'memory';
export const macHasTouchBar = 'hasTouchBar';
