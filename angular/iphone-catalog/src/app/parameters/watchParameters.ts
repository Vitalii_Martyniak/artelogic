export const watchGeneration = 'generation';
export const watchModel = 'model';
export const watchSize = 'size';
export const watchColor = 'color';
export const watchStrapSeries = 'strapSeries';
export const watchStrapColor = 'strapColor';
