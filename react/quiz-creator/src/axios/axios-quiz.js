import axios from 'axios'

export default axios.create({
    baseURL: 'https://quiz-creator-9e626.firebaseio.com'
})