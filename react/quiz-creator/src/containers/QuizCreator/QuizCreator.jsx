import React, {Component} from "react";
import classes from './QuizCreator.css';
import {connect} from "react-redux";
import Button from '../../components/UI/Button/Button.jsx';
import {createQuizQuestion,
    sendNewQuiz,
    changeHasQuestionsAndOptions,
    updateFormControls,
    setRightAnswerId} from '../../store/actions/quizCreator'
import QuizCreatorControls from "../../components/QuizCreatorControls/QuizCreatorControls.jsx";
import {bindActionCreators} from "redux";
import {getNewQuiz, getFormControls, getHasQuestionsAndOptions, getRightAnswerId} from "../../store/selectors/quizCreator";

class QuizCreator extends Component {

    submitHandler = event => {
        event.preventDefault();
    };

    addQuestionHandler = event => {
        event.preventDefault();

        const {newQuiz, rightAnswerId, createQuizQuestion} = this.props;
        const {question, option1, option2, option3, option4} = this.props.formControls;

        const questionItem = {
            question: question.value,
            questionId: newQuiz.length + 1,
            rightAnswerId: rightAnswerId,
            userAnswerId: 0,
            answers: [
                {text: option1.value, id: option1.id},
                {text: option2.value, id: option2.id},
                {text: option3.value, id: option3.id},
                {text: option4.value, id: option4.id},
            ]
        };

        createQuizQuestion(questionItem);
    };

    createQuizHandler = event => {
        event.preventDefault();

        this.props.sendNewQuiz();
    };

    changeHandler = (value, controlName) => { // setting our question options into formControls
        const {changeHasQuestionsAndOptions, updateFormControls, formControls} = this.props;
        const copyFormControls = { ...formControls };
        const control = { ...copyFormControls[controlName] };

        control.value = value;
        copyFormControls[controlName] = control;

        updateFormControls(copyFormControls);

        let booleanArray = [];
        let validation = false;

        Object.keys(copyFormControls).forEach(item => { // checking if all question and options have value
            const booleanValue = !!copyFormControls[item].value.trim();
            booleanArray.push(booleanValue);
        });

        if (booleanArray.toString() === "true,true,true,true,true") {
            validation = true;
        }

        changeHasQuestionsAndOptions(validation);
    };

    checkboxChangeHandler = event => {
        this.props.setRightAnswerId(parseInt(event.target.value));
        event.target.checked = false;
    };

    iconChangeHandler = controlName => { // changing checked icon depend on which was clicked
        const {formControls, updateFormControls} = this.props;
        const copyFormControls = { ...formControls };
        const control = { ...copyFormControls[controlName] };

        Object.keys(copyFormControls).forEach(item => { // checked state for every icon is set to false
            copyFormControls[item].checked = false;
        });

        control.checked = true;
        copyFormControls[controlName] = control;

        updateFormControls(copyFormControls);
    };

    render() {
        const {newQuiz, hasQuestionsAndOptions, rightAnswerId, formControls} = this.props;
        return (
            <div className={classes.QuizCreator}>
                <div className={classes.container}>
                    <h1 className={classes.title}>Quiz Creator</h1>

                    <form onSubmit={this.submitHandler}>

                        <QuizCreatorControls
                            formControls={formControls}
                            changeHandler={this.changeHandler}
                            checkboxChangeHandler={this.checkboxChangeHandler}
                            iconChangeHandler={this.iconChangeHandler}
                        />

                        <div className={classes.buttonContainer}>

                            <p>Quiz must contain at least 5 questions and max is 15 questions.</p>
                            <Button
                                type="primary"
                                onClick={this.addQuestionHandler}
                                disabled={newQuiz.length === 15 || !hasQuestionsAndOptions || rightAnswerId === null}
                            >
                                Add question
                            </Button>

                            <Button
                                type="success"
                                onClick={this.createQuizHandler}
                                disabled={newQuiz.length < 5}
                            >
                                Create quiz
                            </Button>
                            <span>Questions in quiz <b>{newQuiz.length}</b> / 15</span>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        newQuiz: getNewQuiz(state),
        formControls: getFormControls(state),
        hasQuestionsAndOptions: getHasQuestionsAndOptions(state),
        rightAnswerId: getRightAnswerId(state),
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        ...bindActionCreators({ createQuizQuestion, sendNewQuiz,
            changeHasQuestionsAndOptions, updateFormControls, setRightAnswerId }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(QuizCreator)
