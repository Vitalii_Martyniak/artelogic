import React, {Component} from "react";
import classes from './QuizList.css'
import {connect} from "react-redux";
import {NavLink} from "react-router-dom";
import Loader from "../../components/UI/Loader/Loader.jsx";
import {getQuizes} from "../../store/actions/quiz";
import {bindActionCreators} from "redux";
import {getLoading, getStateQuizes} from "../../store/selectors/quiz";
import uuid from "uuid";

class QuizList extends Component {

    componentDidMount() {
        this.props.getQuizes();
    }

    render() {
        const {loading, quizes} = this.props;
        return (
            <div className={classes.QuizList}>
                <div>
                    <h1 className={classes.title}>Quiz List</h1>

                    {
                        loading
                            ? <Loader />
                            : <ul className={classes.list}>
                                {
                                    quizes.map(quiz => (
                                        <li
                                            className={classes.listItem}
                                            key={uuid()}>
                                            <NavLink className={classes.link} to={'/quiz/' + quiz.id}>
                                                {quiz.name}
                                            </NavLink>
                                        </li>
                                    ))
                                }
                            </ul>
                    }
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        quizes: getStateQuizes(state),
        loading: getLoading(state)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        ...bindActionCreators({ getQuizes }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(QuizList)
