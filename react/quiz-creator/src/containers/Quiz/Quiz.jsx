import React, {Component} from "react";
import classes from './Quiz.css'
import ActiveQuizQuestion from "../../components/ActiveQuizQuestion/ActiveQuizQuestion.jsx";
import FinishedQuiz from "../../components/FinishedQuiz/FinishedQuiz.jsx";
import { bindActionCreators } from "redux";
import {connect} from 'react-redux'
import {getQuizById,
    onAnswerClickHandler,
    retryHandler} from '../../store/actions/quiz'
import Loader from "../../components/UI/Loader/Loader.jsx";
import {getQuiz, getActiveQuestionNumber, getIsFinished,
    getLoading, getResults, getRightAnswersCounter} from "../../store/selectors/quiz";

class Quiz extends Component {

    componentDidMount() {
        const {getQuizById, match} = this.props;
        getQuizById(match.params.id);
    }

    componentWillUnmount() {
        this.props.retryHandler();
    }

    render() {
        const {loading, quiz, isFinished, rightAnswersCounter, results,
            activeQuestionNumber, retryHandler, onAnswerClickHandler} = this.props;
        return (
            <div className={classes.Quiz}>
                <div>
                    <h1 className={classes.title}>Answer the questions below</h1>

                    {
                        loading || !quiz
                        ? <Loader/>
                        : isFinished
                            ? <FinishedQuiz
                                rightAnswersCounter={rightAnswersCounter}
                                results={results}
                                quiz={quiz}
                                onRetry={retryHandler}
                            />
                            : <ActiveQuizQuestion
                                answers = {quiz[activeQuestionNumber].answers}
                                question = {quiz[activeQuestionNumber].question}
                                quiz={quiz}
                                onAnswerClick={onAnswerClickHandler}
                                activeQuestionNumber={activeQuestionNumber + 1}
                            />
                    }
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        quiz: getQuiz(state),
        activeQuestionNumber: getActiveQuestionNumber(state),
        isFinished: getIsFinished(state),
        rightAnswersCounter: getRightAnswersCounter(state),
        results: getResults(state),
        loading: getLoading(state),
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        ...bindActionCreators({ getQuizById, onAnswerClickHandler, retryHandler }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Quiz)
