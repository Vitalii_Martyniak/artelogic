import {combineReducers} from "redux";
import quizReducer from "./quiz";
import quizCreatorReducer from "./quizCreator";

export default combineReducers({
    quiz: quizReducer,
    quizCreator: quizCreatorReducer
})