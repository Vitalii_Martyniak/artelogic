import {
    TOGGLE_MENU_HANDLER,
    FINISH_QUIZ,
    GET_QUIZ_SUCCESS,
    SHOW_LOADER,
    HIDE_LOADER,
    GET_QUIZES_SUCCESS,
    QUIZ_ADD_RIGHT_ANSWERS_COUNTER,
    QUIZ_NEXT_QUESTION,
    SET_NEW_RESULTS,
    RETRY_HANDLER,
} from "../actions/actionTypes";

const initialState = {
    //Layout
    menu: false,
    // QuizList
    quizes: [],
    loading: false,
    // Quiz
    results: [],
    isFinished: false,
    rightAnswersCounter: 0,
    activeQuestionNumber: 0,
    quiz: null,
};

export default function quizReducer(state = initialState, action) {
    switch (action.type) {
        case TOGGLE_MENU_HANDLER:
            return {
                ...state,
                menu: !state.menu
            };
        case SHOW_LOADER:
            return {
                ...state,
                loading: true
            };
        case GET_QUIZES_SUCCESS:
            return {
                ...state,
                loading: false,
                quizes: action.quizes
            };
        case HIDE_LOADER:
            return {
                ...state,
                loading: false,
            };
        case GET_QUIZ_SUCCESS:
            return {
                ...state,
                loading: false,
                quiz: action.quiz
            };
        case QUIZ_ADD_RIGHT_ANSWERS_COUNTER:
            return {
                ...state,
                rightAnswersCounter: state.rightAnswersCounter + 1
            };
        case FINISH_QUIZ:
            return {
                ...state,
                isFinished: true
            };
        case QUIZ_NEXT_QUESTION:
            return {
                ...state,
                activeQuestionNumber: state.activeQuestionNumber + 1
            };
        case SET_NEW_RESULTS:
            return {
                ...state,
                results: action.results
            };
        case RETRY_HANDLER:
            return {
                ...state,
                results: [],
                isFinished: false,
                rightAnswersCounter: 0,
                activeQuestionNumber: 0
            };
        default:
            return state
    }
}
