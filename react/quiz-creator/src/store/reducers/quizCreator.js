import {
    SET_RIGHT_ANSWER_ID,
    UPDATE_FORM_CONTROLS,
    CREATE_QUIZ_QUESTION,
    RESET_NEW_QUIZ,
    CHANGE_HAS_QUESTIONS_AND_OPTIONS
} from "../actions/actionTypes";

const initialState = {
    newQuiz: [],
    formControls: createFormControls(),
    hasQuestionsAndOptions: false,
    rightAnswerId: null
};

function createFormControls() {
    return {
        question: {
            label: 'Add question',
            value: ''
        },
        option1: createOptionControl(1),
        option2: createOptionControl(2),
        option3: createOptionControl(3),
        option4: createOptionControl(4),
    }
}

function createOptionControl(number) {
    return {
        label: `Add option ${number}`,
        value: '',
        id: number,
        checked: false
    }
}

export default function quizCreatorReducer(state = initialState, action) {
    switch (action.type) {
        case CHANGE_HAS_QUESTIONS_AND_OPTIONS:
            return {
                ...state,
                hasQuestionsAndOptions: action.boolean
            };
        case SET_RIGHT_ANSWER_ID:
            return {
                ...state,
                rightAnswerId: action.rightAnswerId
            };
        case UPDATE_FORM_CONTROLS:
            return {
                ...state,
                formControls: action.formControls
            };
        case CREATE_QUIZ_QUESTION:
            return {
                ...state,
                newQuiz: [...state.newQuiz, action.questionItem],
                rightAnswerId: null,
                formControls: createFormControls(),
                hasQuestionsAndOptions: false
            };
        case RESET_NEW_QUIZ:
            return {
                ...state,
                newQuiz: [],
                formControls: createFormControls()
            };
        default:
            return state
    }
}
