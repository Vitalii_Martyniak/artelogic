import {
    TOGGLE_MENU_HANDLER,
    RETRY_HANDLER,
    SHOW_LOADER,
    HIDE_LOADER,
    GET_QUIZES_SUCCESS,
    GET_QUIZ_SUCCESS,
    QUIZ_ADD_RIGHT_ANSWERS_COUNTER,
    FINISH_QUIZ,
    QUIZ_NEXT_QUESTION,
    SET_NEW_RESULTS
} from "./actionTypes";
import axios from '../../axios/axios-quiz'

//Menu

export function toggleMenuHandler() {
    return {
        type: TOGGLE_MENU_HANDLER
    }
}

// QuizList
export function getQuizes() {
    return async dispatch => {
        dispatch(showLoader());
        try {
            const response = await axios.get('/quizes.json');
            const quizes = [];

            Object.keys(response.data).forEach((key, index) => {
                quizes.push({
                    id: key,
                    name: `Quiz №${index + 1}`
                })
            });

            dispatch(getQuizesSuccess(quizes));

        } catch (error) {
            dispatch(hideLoader(error));
        }
    }
}

export function getQuizById(quizId) {
    return async dispatch => {
        dispatch(showLoader());

        try {
            const response = await axios.get(`/quizes/${quizId}.json`);
            const quiz = response.data;

            dispatch(getQuizSuccess(quiz))
        } catch (error) {
            dispatch(hideLoader(error))
        }
    }
}

export function getQuizSuccess(quiz) {
    return {
        type: GET_QUIZ_SUCCESS,
        quiz
    }
}

export function showLoader() {
    return {
        type: SHOW_LOADER
    }
}

export function getQuizesSuccess(quizes) {
    return {
        type: GET_QUIZES_SUCCESS,
        quizes
    }
}

export function hideLoader(error) {
    return {
        type: HIDE_LOADER,
        error
    }
}

// Quiz
export function onAnswerClickHandler(answerId) {
    return (dispatch, getState) => {
        const {quiz, activeQuestionNumber, results} = getState().quiz;
        const question = quiz[activeQuestionNumber];
        const copyResults = results;

        question.userAnswerId = answerId;

        if (question.userAnswerId === question.rightAnswerId) {
            dispatch(quizAddRightAnswersCounter());
        }

        copyResults.push(question.userAnswerId);
        dispatch(setNewResults(copyResults));

        if (activeQuestionNumber + 1 === quiz.length) {
            dispatch(finishQuiz());
        } else {
            dispatch(quizNextQuestion());
        }
    }
}

export function quizAddRightAnswersCounter() {
    return {
        type: QUIZ_ADD_RIGHT_ANSWERS_COUNTER
    }
}

export function finishQuiz() {
    return {
        type: FINISH_QUIZ
    }
}

export function quizNextQuestion() {
    return {
        type: QUIZ_NEXT_QUESTION
    }
}

export function setNewResults(results) {
    return {
        type: SET_NEW_RESULTS,
        results
    }
}

export function retryHandler() {
    return {
        type: RETRY_HANDLER
    }
}
