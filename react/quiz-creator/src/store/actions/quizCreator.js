import {
    SET_RIGHT_ANSWER_ID,
    UPDATE_FORM_CONTROLS,
    CREATE_QUIZ_QUESTION,
    RESET_NEW_QUIZ,
    CHANGE_HAS_QUESTIONS_AND_OPTIONS
} from "./actionTypes";
import axios from '../../axios/axios-quiz'

export function setRightAnswerId(rightAnswerId) {
    return {
        type: SET_RIGHT_ANSWER_ID,
        rightAnswerId
    }
}

export function updateFormControls(formControls) {
    return {
        type: UPDATE_FORM_CONTROLS,
        formControls
    }
}

export function changeHasQuestionsAndOptions(boolean) {
    return {
        type: CHANGE_HAS_QUESTIONS_AND_OPTIONS,
        boolean
    }
}

export function createQuizQuestion(questionItem) {
    return {
        type: CREATE_QUIZ_QUESTION,
        questionItem
    }
}

export function sendNewQuiz() {
    return async (dispatch, getState) => {
        await axios.post('/quizes.json', getState().quizCreator.newQuiz);
        dispatch(resetNewQuiz())
    }
}

export function resetNewQuiz() {
    return {
        type: RESET_NEW_QUIZ
    }
}
