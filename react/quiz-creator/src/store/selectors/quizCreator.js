export const getNewQuiz = state => state.quizCreator.newQuiz;

export const getFormControls = state => state.quizCreator.formControls;

export const getHasQuestionsAndOptions = state => state.quizCreator.hasQuestionsAndOptions;

export const getRightAnswerId = state => state.quizCreator.rightAnswerId;
