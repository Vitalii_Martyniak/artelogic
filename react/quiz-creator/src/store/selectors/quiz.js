// Quiz

export const getQuiz = state => state.quiz.quiz;

export const getActiveQuestionNumber = state => state.quiz.activeQuestionNumber;

export const getIsFinished = state => state.quiz.isFinished;

export const getRightAnswersCounter = state => state.quiz.rightAnswersCounter;

export const getResults = state => state.quiz.results;

export const getLoading = state => state.quiz.loading;

// Quiz List

export const getStateQuizes = state => state.quiz.quizes;

// Layout

export const getMenu = state => state.quiz.menu;
