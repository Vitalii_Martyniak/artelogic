import React from "react";
import classes from './Loader.css'

const Loader = () => (
    <div className={classes.centered}>
        <div className={classes.Loader}/>
    </div>
);

export default Loader