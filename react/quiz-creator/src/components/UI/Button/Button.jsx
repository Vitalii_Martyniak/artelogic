import React from "react";
import classes from './Button.css'

const Button = ({type, onClick, disabled, children}) => (
    <button
        onClick={onClick}
        className={`${classes.Button} ${classes[type]}`}
        disabled={disabled}
    >
        { children }
    </button>
);

export default Button
