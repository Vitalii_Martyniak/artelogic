import React from "react";
import classes from './FinishedQuiz.css'
import QuestionRegex from "../QuestionRegex/QuestionRegex.jsx";
import Button from "../UI/Button/Button.jsx";
import {Link} from "react-router-dom";
import uuid from "uuid";

const FinishedQuiz = ({quiz, rightAnswersCounter, onRetry}) => (
    <div className={classes.FinishedQuiz}>

            { quiz.map((quizItem, index) => {
                return (
                    <div className={classes.quizItem} key={uuid()}>
                        <p><b>{index + 1}.&nbsp;
                            <QuestionRegex question={quizItem.question}/>
                        </b></p>
                        {quizItem.userAnswerId !== quizItem.rightAnswerId
                         &&
                            <p>{quizItem.answers[quizItem.userAnswerId - 1].text}<i className={`${classes.icon} fa fa-times ${classes.fail}`}/></p>
                        }
                        <p>{quizItem.answers[quizItem.rightAnswerId - 1].text}<i className={`${classes.icon} fa fa-check ${classes.success}`}/></p>
                    </div>
                )
            }) }

        <p>Right answers: {rightAnswersCounter} from {quiz.length}</p>

        <div>
            <Button onClick={onRetry} type="primary">Try again</Button>
            <Link to="/">
                <Button type="success" onClick={onRetry}>Go to quiz list</Button>
            </Link>
        </div>
    </div>
);

export default FinishedQuiz
