import React from "react";
import classes from './ActiveQuizQuestion.css'
import AnswersList from "./AnswersList/AnswersList.jsx";
import QuestionRegex from "../QuestionRegex/QuestionRegex.jsx";

const ActiveQuizQuestion = ({activeQuestionNumber, question, quiz, answers, onAnswerClick}) => (
    <div className={classes.ActiveQuizQuestion}>
        <p className={classes.Question}>
        <span>
            <strong>{activeQuestionNumber}.</strong>&nbsp;
            <QuestionRegex question={question}/>
        </span>
            <span>{activeQuestionNumber} from {quiz.length}</span>
        </p>

        <AnswersList
            answers={answers}
            onAnswerClick={onAnswerClick}
        />
    </div>
);

export default ActiveQuizQuestion
