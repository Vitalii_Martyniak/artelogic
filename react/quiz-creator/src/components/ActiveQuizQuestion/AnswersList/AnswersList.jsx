import React from "react";
import classes from './AnswersList.css'
import AnswerItem from "./AnswerItem/AnswerItem.jsx";
import uuid from "uuid";

const AnswersList = ({answers, onAnswerClick}) => (
    <ul className={classes.AnswersList}>
        {answers.map(answer => {
            return (
                <AnswerItem
                    key={uuid()}
                    answer={answer}
                    onAnswerClick={onAnswerClick}
                />
            )
        })}
    </ul>
);

export default AnswersList
