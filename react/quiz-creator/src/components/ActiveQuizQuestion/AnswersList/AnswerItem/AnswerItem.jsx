import React from "react";
import classes from './AnswerItem.css'

const AnswerItem = ({onAnswerClick, answer}) => (
    <li
        className={classes.AnswerItem}
        onClick={() => onAnswerClick(answer.id)}
    >
        { answer.text }
    </li>
);

export default AnswerItem
