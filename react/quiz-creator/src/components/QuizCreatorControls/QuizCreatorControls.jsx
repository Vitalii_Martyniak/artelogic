import React from "react";
import classes from './QuizCreatorControls.css'

const QuizCreatorControls = ({formControls, changeHandler, iconChangeHandler, checkboxChangeHandler}) => (
    Object.keys(formControls).map((controlName, index) => {
        const control = formControls[controlName];

        if(!!!index) {
            return (
                <React.Fragment key={controlName}>
                    <label>{control.label}</label>
                    <input value={control.value}
                           className={classes.questionInput}
                           placeholder="Example: `What weather was yesterday in Lviv?`"
                           onChange={event => changeHandler(event.target.value, controlName)}
                    />
                    <hr />
                </React.Fragment>
            )
        }

        return (
            <div className={classes.formElement} key={controlName}>
                <label className={classes.questionLabel}><b>{control.label}</b></label>
                <input value={control.value}
                       className={classes.questionInput}
                       onChange={event => changeHandler(event.target.value, controlName)}
                />
                <input id={index} value={index} type="checkbox" name="rightAnswer"
                       onChange={event => checkboxChangeHandler(event)}
                />
                <label htmlFor={index}
                       onClick={() => iconChangeHandler(controlName)}
                >Right answer?
                    <i className={formControls[controlName].checked ? 'far fa-check-circle' : 'far fa-circle'}/>
                </label>
            </div>
        )
    })
);

export default QuizCreatorControls
