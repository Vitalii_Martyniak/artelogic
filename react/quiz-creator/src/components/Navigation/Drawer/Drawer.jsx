import React from "react";
import classes from './Drawer.css'
import {NavLink} from 'react-router-dom'
import Backdrop from "../../UI/Backdrop/Backdrop.jsx";
import uuid from "uuid";

const Drawer = ({isOpen, onClose}) => {
    const linksTo = [{link: '/', text: 'Quiz List'}, {link: '/quiz-creator', text: 'Quiz Creator'}];

    return (
        <React.Fragment>
            <nav className={isOpen ? classes.Drawer : `${classes.Drawer} ${classes.close}`}>
                <ul className={classes.list}>

                    { linksTo.map((item, index) => {
                        return (
                            <li className={classes.listItem} key={uuid()}>
                                <NavLink
                                    to={item.link}
                                    exact
                                    className={classes.link}
                                    activeClassName={classes.active}
                                    onClick={onClose}
                                >
                                    {item.text}
                                </NavLink>
                            </li>
                        )
                    }) }

                </ul>
            </nav>
            { isOpen && <Backdrop onClick={onClose}/>}
        </React.Fragment>
    )
};

export default Drawer
