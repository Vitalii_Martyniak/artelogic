import React from "react";
import classes from './MenuToggle.css'

const MenuToggle = ({isOpen, onToggle}) => (
    <i
        className={isOpen ?
            `${classes.MenuToggle} fa fa-times ${classes.open}`
            : `${classes.MenuToggle} fa fa-bars`}
        onClick={onToggle}
    />
);

export default MenuToggle
