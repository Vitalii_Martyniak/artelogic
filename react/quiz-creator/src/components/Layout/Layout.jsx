import React from "react";
import classes from './Layout.css'
import MenuToggle from "../Navigation/MenuToggle/MenuToggle.jsx";
import Drawer from "../Navigation/Drawer/Drawer.jsx";

const Layout = ({menu, children, toggleMenuHandler}) => (
    <div className={classes.Layout}>

        <Drawer
            isOpen={menu}
            onClose={toggleMenuHandler}
        />

        <MenuToggle
            isOpen={menu}
            onToggle={toggleMenuHandler}
        />

        {children}
    </div>
);

export default Layout;
