import React from "react";
import uuid from "uuid";

const QuestionRegex = ({question}) => (
    question.split(/["'`](.+?)["'`]/).map((text, index) => (
        index % 2 ? (<code key={uuid()}>{text}</code>) : text)
    )
);

export default QuestionRegex
