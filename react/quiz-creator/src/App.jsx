import React, {Component} from 'react';
import './App.css'
import {Route, Switch} from 'react-router-dom'
import Layout from "./components/Layout/Layout.jsx";
import QuizList from "./containers/QuizList/QuizList.jsx";
import QuizCreator from "./containers/QuizCreator/QuizCreator.jsx";
import Quiz from "./containers/Quiz/Quiz.jsx";
import {toggleMenuHandler} from "./store/actions/quiz";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getMenu} from "./store/selectors/quiz";

class App extends Component{
    render() {
        const {menu, toggleMenuHandler, children} = this.props;
        return (
            <Layout
                menu={menu}
                toggleMenuHandler={toggleMenuHandler}
                children={children}
            >
                <Switch>
                    <Route path="/quiz-creator" component={QuizCreator}/>
                    <Route path="/quiz/:id" component={Quiz}/>
                    <Route path="/" component={QuizList}/>
                </Switch>
            </Layout>
        );
    }
}

function mapStateToProps(state) {
    return {
        menu: getMenu(state)
    }
}

function mapDispatchToProps(dispatch) {
    return {
        dispatch,
        ...bindActionCreators({ toggleMenuHandler }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
