const todos = {
    current: [],
    archive: []
};

const getElement = className => document.querySelector(className);

const toDoListContainer = getElement('.todo-list__container');
const archiveToDoListContainer = getElement('.archive-todo-list__container');
const toDoList = getElement('.todo-list__list');
const archiveToDoList = getElement('.archive-todo-list__archive-list');
const addToDoField = getElement('.add-todo__field');

document.addEventListener('click',(e) => {
    switch (e.target.className) {
        case 'add-todo__button':
            addToDo();
            break;
        case 'to-archive-todo-list__button':
            toDoListContainer.classList.add('d-none');
            archiveToDoListContainer.classList.remove('d-none');
            createArchiveToDoList();
            break;
        case 'fas fa-long-arrow-alt-left to-todo-list__button_icon':
            toDoListContainer.classList.remove('d-none');
            archiveToDoListContainer.classList.add('d-none');
            deleteAllArchiveList();
            break;
        case 'list-item__checkbox':
            checkboxToggle(e.target);
            break;
        case 'list-item__archive-button archive-btn-active':
            archiveList(e.target);
            break;
        case 'archive-list-item__delete-button':
            deleteList(e.target);
    }
});

const makeId = () => {
    return Math
        .random()
        .toString(36)
        .substr(2, 16);
};

const createElement = (parentElement, tagName, textContent, className, attributeName, attributeValue) => {
    const element = document.createElement(tagName);
    element.textContent = textContent;
    if (Array.isArray(className)) {
        className.forEach( classItem => {
            element.classList.add(classItem);
        });
    } else {
        element.classList.add(className);
    }
    element.setAttribute(attributeName, attributeValue);
    parentElement.appendChild(element);
    return element;
};

const addToDo = () => {
    if (addToDoField.value === '') {
        alert('Enter your todo!');
        return;
    }

    const newTodo = {
        text: addToDoField.value,
        id: makeId()
    };
    todos.current.push(newTodo);

    const listItem = createElement(toDoList, 'li', null, 'todo-list__list-item', 'data-id', newTodo.id);

    createElement(listItem, 'div', todos.current.length + '.','list-item__number');

    createElement(listItem, 'div', addToDoField.value, 'list-item__text');

    const checkboxLabel = createElement(listItem, 'label', null, 'list-item__checkbox-label');

    createElement(checkboxLabel, 'i', null, ['far', 'fa-square']);

    createElement(checkboxLabel, 'input', null, 'list-item__checkbox',
        'type', 'checkbox');

    createElement(listItem, 'div', 'in Progress', 'list-item__progress');

    createElement(listItem, 'button', 'Archive', 'list-item__archive-button',
        'disabled', true);

    addToDoField.value = '';
};

const checkboxToggle = (checkbox) => {
    debugger;
    const listItem = checkbox.parentNode.parentNode;
    const archiveBtn = listItem.querySelector('.list-item__archive-button');
    const progress = listItem.querySelector('.list-item__progress');
    if (checkbox.checked) {
        const labelIcon = listItem.querySelector('.fa-square');
        archiveBtn.disabled = false;
        archiveBtn.classList.add('archive-btn-active');
        progress.classList.add('progress-done');
        progress.textContent = 'Done';
        labelIcon.classList.remove('fa-square');
        labelIcon.classList.add('fa-check-square');
    } else {
        const labelIcon = listItem.querySelector('.fa-check-square');
        archiveBtn.disabled = true;
        archiveBtn.classList.remove('archive-btn-active');
        progress.classList.remove('progress-done');
        progress.textContent = 'in Progress';
        labelIcon.classList.remove('fa-check-square');
        labelIcon.classList.add('fa-square');
    }
};

const archiveList = (archiveBtn) => {
    const listItem = archiveBtn.parentNode;
    todos.current.forEach((todo, index) => {
        if (listItem.dataset.id === todo.id) {
            todos.archive.push(todo);
            todos.current.splice(index, 1);
        }
    });
    listItem.remove();
    updateNumbers();
};

const updateNumbers = () => {
    const numbers = document.querySelectorAll('.list-item__number');
    numbers.forEach((number, index) => {
        number.textContent = index + 1 + '.';
    });
};

const createArchiveToDoList = () => {
    todos.archive.forEach(todo => {
        const listItem = createElement(archiveToDoList, 'li', null, 'archive-list__archive-list-item', 'data-id', todo.id);

        createElement(listItem, 'div', todo.text, 'archive-list-item__text');

        createElement(listItem, 'button', 'Delete', 'archive-list-item__delete-button');
    });
};

const deleteList = (deleteBtn) => {
    const listItem = deleteBtn.parentNode;
    todos.archive.forEach((todo, index) => {
        if (listItem.dataset.id === todo.id) {
            todos.archive.splice(index, 1);
        }
    });
    listItem.remove();
};

const deleteAllArchiveList = () => {
    const allArchiveList = document.querySelectorAll('.archive-list__archive-list-item');
    allArchiveList.forEach(li => li.remove());
};
