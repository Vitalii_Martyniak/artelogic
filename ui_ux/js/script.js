const macDisplay = document.querySelector('.mac__display');
const screenPosition = window.innerHeight / 2;

const displayAppear = () => {
    const macDisplayPosition = macDisplay.getBoundingClientRect().top;

    if (macDisplayPosition < screenPosition) {
        macDisplay.classList.add('appear');
        window.removeEventListener('scroll', displayAppear);
    }
};

window.addEventListener('scroll', displayAppear);